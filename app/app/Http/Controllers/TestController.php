<?php

namespace App\Http\Controllers;

use App\Models\BTreeClient;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    /**
     * @return array
     */
    public function run(): array
    {
        DB::transaction(static function () {
            BTreeClient::factory()->create();
        });

        return [
            'success' => true,
            'data' => []
        ];
    }
}
