<?php

namespace Database\Seeders;

use App\Models\BTreeClient;
use App\Models\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * @var int
     */
    private const BATCH_COUNT = 2000;

    /**
     * @var int
     */
    private const BATCH_SIZE = 5000;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*for ($i = 1; $i <= static::BATCH_COUNT; $i++) {
            DB::transaction(static function () {
                BTreeClient::factory(static::BATCH_SIZE)->create();
            });
        }*/

        for ($i = 1; $i <= static::BATCH_COUNT; $i++) {
            DB::transaction(static function () {
                Client::factory(static::BATCH_SIZE)->create();
            });
        }
    }
}
