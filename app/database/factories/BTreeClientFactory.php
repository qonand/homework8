<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BTreeClientFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'phone' => $this->faker->e164PhoneNumber,
            'birthday' => $this->faker->date
        ];
    }
}
