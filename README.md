# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework8.git`
2. Run `docker-compose up -d nginx mysql workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`
 - `php artisan migrate`
 - `php artisan db:seed` (can take time)

# How to run the application
1. Run `docker-compose up -d nginx mysql workspace` in `laradock` folder
2. Open `http://localhost/api/test/run` in your web browser


# Result of data select
Select with index
![Scheme](select-with-index.png)

Select without index
![Scheme](select-without-index.png)

# Result of data insert
innodb_flush_log_at_trx_commit = 0
![Scheme](innodb_flush_log_at_trx_commit_0.png)

innodb_flush_log_at_trx_commit = 1
![Scheme](innodb_flush_log_at_trx_commit_1.png)

innodb_flush_log_at_trx_commit = 2
![Scheme](innodb_flush_log_at_trx_commit_2.png)